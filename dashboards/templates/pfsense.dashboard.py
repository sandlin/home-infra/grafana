import requests
import pprint
import json

from grafanalib.core import (
    Dashboard, TimeSeries, GaugePanel,
    Target, GridPos,
    OPS_FORMAT
)

PROMETHEUS = 'http://prometheus.h8n.lan:9090'
response =requests.get(PROMETHEUS + '/api/v1/query', params={'query': 'sum by (__name__) ({instance=~"pfsense.*"})'}) 

query_results = json.loads(response.text)

metric_results = query_results['data']['result']

my_panels = []
for mr in metric_results:
    name = mr['metric']['__name__']
    tsp = TimeSeries(
            title=name,
            dataSource='Prometheus',
            targets=[
                Target(
                    expr='rate({}[$__rate_interval])'.format(name),
                    legendFormat="{{ handler }}",
                    refId='A',
                ),
                Target(
                    expr='{}'.format(name),
                    legendFormat="{{ handler }}",
                    refId='A',
                ),
            ],
            unit=OPS_FORMAT,
            gridPos=GridPos(h=8, w=16, x=0, y=10),
        )
    my_panels.append(tsp)


dashboard = Dashboard(
    title="pfsense",
    description="sum by (__name__) ({instance=~\"pfsense.*\"})",
    tags=[
        'pfsense'
    ],
    timezone="browser",
    panels=my_panels,
).auto_panel_ids()