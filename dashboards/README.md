```
$ python genpush.py --template pfsense.dashboard.py
##################################################
Generating Dashboard from pfsense.dashboard.py
##################################################
##################################################
Pushing Dashboard to Grafana
##################################################
##################################################

SUMMARY
----------
dashboard_url: http://grafana.h8n.lan:3000/d/zNbYZmunk/pfsense
grafana_url: grafana.h8n.lan:3000
dashboard: pfsense.dashboard.py.dashboard
```